@extends('adminlte::page')
@section('css')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css"/>
@endsection
@section('content')
    @include('partials.flash-messages')
    <div class="container pt-5">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{$country->country_name}}</div>
                    <div class="card-body">
                        <img class="rounded mx-auto d-block" src="{{asset($country->flag_path)}}" alt="Image"/>
                        <div class="languages">
                            <label class="h2">Języki urzedowe:</label>
                            <ul>
                                @foreach($country->languages()->get() as $language)
                                    <li>{{$language->country_language}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @include('authenticated.partials.table_with_users')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('js/datatables.js') }}"></script>
@endsection
