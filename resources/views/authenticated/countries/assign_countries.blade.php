@extends('adminlte::page')

@section('content')
    @include('partials.flash-messages')
    <div class="container pt-5">
        <div class="card card-success">
            <form action="{{route('users.assignCountryToUser')}}" method="POST">
                @csrf
                <div class="card-header">
                    <h3 class="card-title">Zaznacz odwiedzone kraje!</h3>
                </div>
                <div class="card-body">
                    <!-- Minimal style -->
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- checkbox -->
                            <div class="form-group clearfix">
                                <div class="icheck-primary d-inline">
                                    <input type="checkbox" class="select_all mr-2"  @if($countries->count()==count($userCountries))
                                    checked
                                        @endif>
                                    <label for="select_all">
                                        Zaznacz wszystkie
                                    </label>
                                </div>
                            </div>
                            @foreach($countries as $country)
                                <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                        <input type="checkbox" name="countries[]" id="{{$country->id}}" value="{{$country->id}}"
                                        @if(in_array($country->id,$userCountries))
                                            checked
                                            @endif>
                                        <label for="{{$country->id}}">
                                            <a href="{{route('country.show',$country->id)}}" class="nav-link">
                                                {{$country->country_name}}
                                            </a>
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Zapisz</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('js/assign-country.js') }}"></script>
@endsection
