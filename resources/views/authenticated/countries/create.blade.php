@extends('adminlte::page')

@section('content')
    @include('partials.flash-messages')
    <div class="container pt-5">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Dodawanie kraju </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="quickForm" novalidate="novalidate" action="{{route('country.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="country_name_input">Nazwa kraju</label>
                                <input type="text"  value="{{ old('country_name') }}" name="country_name" class="form-control"
                                       id="country_name_input" placeholder="Nazwa kraju" required>
                                @if($errors->has('country_name'))
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ $errors->first('country_name') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Dodaj flage</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" value="{{ old('image') }}" class="custom-file-input" id="exampleInputFile"
                                               accept="image/png, image/gif, image/jpeg" name="image" required>
                                        <label class="custom-file-label" for="exampleInputFile">Wybierz plik</label>
                                    </div>
                                </div>
                                @if($errors->has('image'))
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ $errors->first('image') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="languages_input">Podaj język urzedowy tego kraju  (jeśli wiecej niż jeden oddziel przecinkami)</label>
                                <input type="text" value="{{ old('languages') }}" name="languages" class="form-control"
                                       id="languages_input" placeholder="Język urzedowy" required>
                                @if($errors->has('languages'))
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ $errors->first('languages') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group mb-0">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="visited" class="custom-control-input" id="visitedCheckbox">
                                    <label class="custom-control-label" for="visitedCheckbox">Odwiedziłem ten kraj!</label>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Zapisz</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection
