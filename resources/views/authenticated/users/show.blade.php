@extends('adminlte::page')
@section('css')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css"/>
@endsection
@section('content')
    @include('partials.flash-messages')
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-10">
                <!-- Widget: user widget style 2 -->
                <div class="card card-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-warning">
                        <div class="widget-user-image">
                            <img class="img-circle elevation-2" src="{{url('/image/person.jpg')}}" alt="User Avatar">
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">{{$user->name}} {{$user->surname}}</h3>
                        <h5 class="widget-user-desc">@ {{$user->login }}</h5>
                    </div>
                    <div class="card-footer p-0">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <label class="nav-link">
                                    Email:<span class="float-right badge bg-danger">{{$user->email }}</span>
                                </label>
                            </li>
                            <li class="nav-item">
                                <label class="nav-link">
                                    Wiek: <span class="float-right badge bg-info">{{$user->getAge() }}</span>
                                </label>
                            </li>
                            <li class="nav-item">
                                <label class="nav-link">
                                    Ilość odwiedzonych krajów: <span
                                        class="float-right badge bg-success">{{$user->countries()->count() }}</span>
                                </label>
                            </li>
                            <li class="nav-item">
                                <label class="nav-link">
                                    Odwiedzone kraje:
                                </label>
                            </li>
                            @foreach($countries as $country)
                                <li class="nav-item">
                                    <a href="{{route('country.show',$country->id)}}" class="nav-link">
                                        {{$country->country_name}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('js/datatables.js') }}"></script>
@endsection
