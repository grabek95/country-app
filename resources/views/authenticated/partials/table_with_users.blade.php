<div class="card">
    <div class="card-header text-center">
        <h2 class="card-title">Lista użytkowników którzy odwiedzili ten kraj</h2>
    </div>
    <div class="card-body table-responsive p-0 auto-height">
        <table id="data-table" class="table table-head-fixed text-nowrap">
            <thead>
            <tr>
                <th>#</th>
                <th>Login</th>
                <th>Imie</th>
                <th>Nazwisko</th>
                <th>Email</th>
                <th>Wiek</th>
            </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td class="id"><label>{{$loop->iteration}}</label></td>
                        <td class="login"><label><a href="{{route('users.show',$user->id)}}">{{$user->login}}</a></label></td>
                        <td class="name"><label>{{$user->name}}</label></td>
                        <td class="surname"><label>{{$user->surname}}</label></td>
                        <td class="email"><label>{{$user->email}}</label></td>
                        <td class="age"><label>{{$user->getAge()}}</label></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
