<div class="col-12">
    <a href="{{route('country.export')}}" class="btn btn-primary mb-2">
        Eksportuj kraje
    </a>
    <div class="card">
        <div class="card-header text-center">
            <h2 class="card-title">Lista krajów z iloscią odwiedzin</h2>
        </div>
        <div class="card-body table-responsive p-0 auto-height">
            <table id="data-table" class="table table-head-fixed text-nowrap">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Kraj</th>
                    <th>Ilość odwiedzin</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($countries))
                    @foreach($countries as $country)
                        <tr >
                            <td class="id"><label>{{$loop->iteration}}</label></td>
                            <td class="country"><label><a href="{{route('country.show',$country->id)}}">{{$country->country_name}}</a></label></td>
                            <td class="count"><label>{{$country->users()->count()}}</label></td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

