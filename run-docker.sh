docker rm $(docker ps -a -q) -f #removes your containers
docker-compose up -d #running your containers
docker exec -it Country-app composer install
docker exec -it Country-app php artisan config:cache
docker exec -it Country-app php artisan config:clear
docker exec -it Country-app php artisan cache:clear
docker exec -it Country-app php artisan view:clear
docker exec -it Country-app php artisan route:clear
docker exec -it Country-app php artisan key:generate
docker exec -it Country-app rm bootstrap/cache/*.php -f
docker exec -it Country-app php artisan migrate
