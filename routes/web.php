<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\CountryController::class, 'index']);
Route::get('/country/export', [App\Http\Controllers\CountryController::class, 'exportCountries'])->name('country.export');


Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/countries', [App\Http\Controllers\CountryController::class, 'index'])->name('country.index');
    Route::get('/country/create', [App\Http\Controllers\CountryController::class, 'create'])->name('country.create');
    Route::get('/country/assignCountryToUser', [App\Http\Controllers\CountryController::class, 'assignCountryToUser'])->name('country.assignCountryToUser');
    Route::post('/country/store-country', [App\Http\Controllers\CountryController::class, 'store'])->name('country.store');
    Route::get('/country/{id}', [App\Http\Controllers\CountryController::class, 'show'])->name('country.show');

    Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
    Route::post('/users/assign-country-to-user/', [App\Http\Controllers\UserController::class, 'assignCountryToUser'])->name('users.assignCountryToUser');
    Route::get('/users/{id}', [App\Http\Controllers\UserController::class, 'show'])->name('users.show');
});
