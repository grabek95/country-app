$(document).ready(function () {
    $('.select_all').change(function (event) {
        if (this.checked) {

            $(this).attr('checked', 'checked');
            $(this).parent().parent().parent().find(':checkbox').each(function () {
                $(this).prop('checked', true);
            });
        } else {
            $(this).parent().parent().parent().find(':checkbox').each(function () {
                $(this).prop('checked', false);
            });
        }
    });
    $(':checkbox').not('.select_all').change(function () {
        let checkedCount = $(this).parent().parent().parent().find(':checkbox:checked ').length;
        let checkboxCount = $(this).parent().parent().parent().find(':checkbox ').length - 1
        console.log(checkboxCount,checkboxCount)
        if ($(this).parent().parent().parent().find('.select_all').prop('checked')) checkedCount -= 1;
        if (checkedCount == checkboxCount && checkedCount) {
            $(this).parent().parent().parent().find('.select_all').prop('checked', true);
        } else {
            $(this).parent().parent().parent().find('.select_all').prop('checked', false);


        }
    });
});
