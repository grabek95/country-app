<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Instrukcja uruchomienia projektu

Aby uruchomic project należy:

- skopiować plik .env.example do gównego katalogu pod nazwą .env
- plik .env.example zawie konfiguracje do testowej poczty gmail - dziala veryfikacja uzytkowników i przywracanie hasla
- posiadac zainstalowanego dockera
- odpalic skrypt run-docker.sh i w przeglądarce wejść pod link localhost:8000
