<?php

namespace App\Providers;

use App\Services\AssignCountryService\AssignCountryService;
use App\Services\AssignCountryService\AssignCountryServiceInterface;
use Illuminate\Support\ServiceProvider;

class AssignCountryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AssignCountryServiceInterface::class, AssignCountryService::class);

    }
}
