<?php

namespace App\Providers;

use App\Services\ExportCollectionService\ExportCollectionService;
use App\Services\ExportCollectionService\ExportCollectionServiceInterface;
use Illuminate\Support\ServiceProvider;

class ExportCollectionProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ExportCollectionServiceInterface::class, ExportCollectionService::class);

    }

}
