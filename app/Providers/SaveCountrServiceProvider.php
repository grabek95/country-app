<?php

namespace App\Providers;

use App\Services\SaveCountryService\SaveCountryService;
use App\Services\SaveCountryService\SaveCountryServiceInterface;
use Illuminate\Support\ServiceProvider;

class SaveCountrServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SaveCountryServiceInterface::class, SaveCountryService::class);
    }

}
