<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function PHPUnit\Framework\returnSelf;

class Country extends Model
{
    use HasFactory;

    protected $guarded =[];
    public function users(){
        return $this->belongsToMany(User::class,'user_visited_country');
    }
    public function languages(){
        return $this->hasMany(CountryLanguage::class);
    }

    public static function getWithCountOfVisits(){
        return self::with('users');
    }
    public static function getMappedCountriesToExport(){
        return self::getWithCountOfVisits()->get()->map(function ($country) {
            return collect([
                'Kraj' => $country->country_name,
                'Liczba odwiedzających' => $country->users->count()
            ]);})->sortByDesc('Liczba odwiedzających');
    }
    public function assignLanguages(array $languages): bool
    {
        return $this->languages()->insert($this->prepareLanguages($languages,$this->id));
    }
    private function prepareLanguages(array $languages, int $countryId)
    {
        return array_map(function ($value) use ($countryId) {
            return ['country_id' => $countryId, 'country_language' => $value];
        }, $languages);
    }
}
