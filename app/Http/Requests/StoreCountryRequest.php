<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCountryRequest extends FormRequest
{
    public function messages()
    {
        return [
            'country_name.required' => 'Nazwa kraju jest wymagana!',
            'country_name.string' => 'Nazwa kraju musi byc napisem!',
            'country_name.unique' => 'Nazwa kraju już istnieje!',
            'image.required'  => 'Zdjęcie flagi jest wymagane!',
            'image.image'  => 'Zdjęcie flagi jest wymagane!',
            'languages.required' => 'Przynajmniej jeden język jest wymagany!',

        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_name' => ['required ',' string ','unique:countries,country_name',' max:255'],
            'image' => ['required','image','max:5000'],
            'languages' => ['required','string','max:255']
        ];
    }
}
