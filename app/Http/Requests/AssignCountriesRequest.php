<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignCountriesRequest extends FormRequest
{

    /**
     * @return string[]
     */
    public function messages()
    {
        return [
            'countries.*.exists' => 'Nie ma takiego kraju!',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'countries.*' => ['exists:countries,id']
        ];
    }
}
