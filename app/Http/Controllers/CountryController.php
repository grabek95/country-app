<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCountryRequest;
use App\Models\Country;
use App\Services\ExportCollectionService\ExportCollectionService;
use App\Services\ExportCollectionService\ExportCollectionServiceInterface;
use App\Services\SaveCountryService\SaveCountryServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CountryController extends Controller
{
    private $saveCountryService;
    private $exportCollectionService;

    public function __construct(SaveCountryServiceInterface $saveCountryService, ExportCollectionServiceInterface $exportCollectionService)
    {
        $this->saveCountryService = $saveCountryService;
        $this->exportCollectionService = $exportCollectionService;
    }

    public function index()
    {
        $view = Auth::check() ? 'authenticated.countries.index' : 'welcome';
        return view($view, [
            'countries' => Country::all()->sortBy('country_name')
        ]);
    }

    public function show(int $id)
    {
        $country = Country::findOrFail($id);
        return view('authenticated.countries.show', [
            'country' => $country,
            'users' => $country->users()->get()
        ]);
    }

    public function create()
    {
        return view('authenticated.countries.create');
    }

    public function assignCountryToUser()
    {
        return view('authenticated.countries.assign_countries', [
            'countries' => Country::all()->sortBy('country_name'),
            'userCountries' => Auth::user()->countries()->pluck('countries.id')->toArray()
        ]);
    }

    public function store(StoreCountryRequest $request)
    {
        $result = $this->saveCountryService->saveCountry($request);
        return redirect()->route('country.index')->with($result['status'], $result['message']);
    }

    public function exportCountries()
    {
        $countriesWithCounts = Country::getMappedCountriesToExport();
        $result = $this->exportCollectionService->exportCollection($countriesWithCounts, 'Kraje');
        session()->now($result['status'], $result['message']);
        session()->reflash();

    }
}
