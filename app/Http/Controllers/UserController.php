<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssignCountriesRequest;
use App\Models\User;
use App\Services\AssignCountryService\AssignCountryServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $assignCountryService;

    public function __construct(AssignCountryServiceInterface $assignCountryService)
    {
        $this->assignCountryService = $assignCountryService;
    }

    public function index()
    {
        return view('authenticated.users.index', [
            'users' => User::all()->sortBy('login')
        ]);
    }

    public function show(Request $request, int $id)
    {
        $user = User::findOrFail($id);
        return view('authenticated.users.show', [
            'user' => $user,
            'countries' => $user->countries()->get()
        ]);
    }

    public function assignCountryToUser(AssignCountriesRequest $request)
    {
        $selectedCountries = $request->get('countries') ?? [];
        $result = $this->assignCountryService->assignCountriesToUser($selectedCountries, Auth::user()->id);

        $user = Auth::user();
        return redirect()->route('users.show', [
            'id' => Auth::user()->id
        ])->with($result['status'], $result['message']);
    }
}
