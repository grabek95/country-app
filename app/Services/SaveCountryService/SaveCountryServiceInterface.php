<?php

namespace App\Services\SaveCountryService;

use Illuminate\Http\Request;

interface SaveCountryServiceInterface
{
    public function saveCountry(Request $requestData):array;
}
