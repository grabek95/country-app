<?php

namespace App\Services\SaveCountryService;

use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SaveCountryService implements SaveCountryServiceInterface
{

    public function saveCountry(Request $requestData): array
    {
        try {
            DB::beginTransaction();
            $imageName = time() . '.' . $requestData->image->extension();
            $requestData->image->move(public_path('flags'), $imageName);
            $path = '/flags/' . $imageName;
            if (file_exists(public_path() . $path)) {
                $country = new Country();
                $country->country_name = $requestData->get('country_name');
                $country->flag_path = $path;
                $languages = explode(',', $requestData->get('languages'));
                $country->save();
                $country->assignLanguages($languages);
                if ($requestData->get('visited')) {
                    DB::table('user_visited_country')->insert(['user_id' => Auth::user()->id, 'country_id' => $country->id]);
                }
            } else {
                throw new Exception("Error!");
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 'error', 'message' => "Wystąpił nieoczekiwany błąd!"];
        }
        return ['status' => 'success', 'message' => "Kraj został zapisany!"];
    }
}
