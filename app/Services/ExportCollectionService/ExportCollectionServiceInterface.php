<?php

namespace App\Services\ExportCollectionService;


use Illuminate\Support\Collection;

interface ExportCollectionServiceInterface
{
    public function exportCollection(Collection $collection, string $filename):array;
}
