<?php

namespace App\Services\ExportCollectionService;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use League\Csv\Writer;

class ExportCollectionService implements ExportCollectionServiceInterface
{
    private $filename;

    public function exportCollection(Collection $collection, string $filename): array
    {
        try {
            $this->filename = $filename;
            $header[] = $this->prepareHeaders($collection);
            $body = $this->prepareBody($collection);
            $this->mergeAndDownload($header, $body);
        } catch (\Exception $e) {
            return ['status' => 'error', 'message' => "Wystąpił nieoczekiwany błąd!"];
        }
        return ['status' => 'success', 'message' => "Plik został wygenerowany!"];
    }

    private function prepareHeaders(Collection $collection): array
    {
        return $collection->first()->keys()->toArray();
    }

    private function prepareBody(Collection $collection): array
    {
        return $collection->toArray();
    }

    private function mergeAndDownload(array $header, array $body)
    {
        $fileName = $this->filename . '-' . Carbon::now()->timestamp . '.csv';
        header('Content-Type: text/csv; charset=UTF-8');
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        $csv = Writer::createFromString();
        $csv->setDelimiter(';');
        $csv->insertAll(array_merge($header, $body));
        $csv->output($fileName);
    }
}
