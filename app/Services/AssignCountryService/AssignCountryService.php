<?php

namespace App\Services\AssignCountryService;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class AssignCountryService implements AssignCountryServiceInterface
{

    public function assignCountriesToUser(array $countriesIds, int $userId): array
    {
        try {
            DB::beginTransaction();
            $user = User::findOrFail($userId);
            DB::table('user_visited_country')->where('user_id', $userId)->delete();
            $countriesToSave = array_map(function ($value) use ($userId) {
                return ['user_id' => $userId, 'country_id' => $value];
            }, $countriesIds);
            if (!empty($countriesToSave)) {
                DB::table('user_visited_country')->insert($countriesToSave);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 'error', 'message' => "Wystąpił nieoczekiwany błąd!"];
        }
        return ['status' => 'success', 'message' => "Przypisano pomyślnie!"];

    }
}
