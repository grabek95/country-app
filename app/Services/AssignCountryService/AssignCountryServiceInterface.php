<?php

namespace App\Services\AssignCountryService;

interface AssignCountryServiceInterface
{
    public function assignCountriesToUser(array $countriesIds, int $userId):array;
}
